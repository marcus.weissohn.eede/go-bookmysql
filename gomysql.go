package main

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

type Book struct {
	Title, Author string
	ID            int
}

func initDb() error {

	db, err := sql.Open("mysql", goDotEnvVariable("MYSQL"))
	if err != nil {
		return err
	}

	_, err = db.Exec("CREATE DATABASE IF NOT EXISTS go_books")
	if err != nil {
		return err
	}

	db.Close()
	println("Database go_books initiated")

	dbT, err := sql.Open("mysql", goDotEnvVariable("DB"))
	if err != nil {
		return err
	}

	_, err = dbT.Exec(`CREATE TABLE IF NOT EXISTS books_libary (
  	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  	title_value VARCHAR(100),
	author_value VARCHAR(100))`)
	if err != nil {
		return err
	}

	dbT.Close()
	println("table books_libary created")

	return nil
}

func connectDb() (*sql.DB, error) {

	db, err := sql.Open("mysql", goDotEnvVariable("DB"))

	return db, err
}

func addBookDb(book Book) error {
	db, err := connectDb()
	if err != nil {
		return err
	}

	_, err = db.Exec("INSERT INTO books_libary (title_value,author_value) VALUES (? ,?)", book.Title, book.Author)

	if err != nil {
		return err
	}

	db.Close()

	println("Added Book")

	return err

}

func readBooksDb() ([]Book, error) {
	db, err := connectDb()
	if err != nil {
		return nil, err
	}

	results, err1 := db.Query("SELECT * FROM books_libary;")

	if err1 != nil {
		return nil, err1
	}

	var books []Book
	for results.Next() {
		var book Book
		err = results.Scan(&book.ID, &book.Title, &book.Author)

		if err != nil {
			return nil, err
		}

		books = append(books, book)

	}

	return books, err

}

func restAddBook(w http.ResponseWriter, r *http.Request) {
	var book Book
	err := json.NewDecoder(r.Body).Decode(&book)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(([]byte)("Error:" + err.Error()))
	}

	if len(book.Title) <= 0 || len(book.Author) <= 0 {
		w.WriteHeader(http.StatusPaymentRequired)
		w.Write(([]byte)("Error:" + "Body not complete"))

		return
	}

	err = addBookDb(book)
	if err != nil {
		w.WriteHeader(http.StatusPaymentRequired)
		w.Write(([]byte)("Error:" + err.Error()))
	}

}

func restReadBooks(w http.ResponseWriter, r *http.Request) {

	books, err := readBooksDb()
	if err != nil {
		w.WriteHeader(int(http.StateHijacked))
		w.Write(([]byte)("Error:" + err.Error()))
	}
	json.NewEncoder(w).Encode(books)

}

func handleRequests() {
	r := mux.NewRouter()
	r.HandleFunc("/addBook", restAddBook)
	r.HandleFunc("/readBooks", restReadBooks)
	srv := &http.Server{
		Handler: r,
		Addr:    "127.0.0.1:8123",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}

func main() {
	initDb()
	handleRequests()
}

// use godot package to load/read the .env file and
// return the value of the key
func goDotEnvVariable(key string) string {

	// load .env file
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	return os.Getenv(key)
}
